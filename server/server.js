const express = require('express');

const socketIO = require("socket.io");

const http = require("http");

const path = require('path');

const { Usuarios } = require("./classes/usuarios");

const { crearMensaje } = require("./utilidades/utilidades");

const app = express();

let server = http.createServer(app);

const publicPath = path.resolve(__dirname, '../public');
const port = process.env.PORT || 3000;

app.use(express.static(publicPath));

let io = socketIO(server);

const usuarios = new Usuarios();

io.on("connection", (client) => {
    console.log("Usuario conectado");

    /*
    client.on("entrarChat", (usuario) => 
        console.log("Usuario conectado ", usuario)
    );
    */

    
    client.on("entrarChat", (data, callback) => {
        if (!data.nombre || !data.sala) {
            return callback({
                error: true,
                mensaje: "El nombre es necesario",
            });
        }

        client.join(data.sala);

        usuarios.agregarPersona(client.id, data.nombre, data.sala);

        //let personas = usuarios.agregarPersona(client.id, data.nombre);
        //client.broadcast.emit("listaPersonas", usuarios.getPersonas());
        client.broadcast.to(data.sala).emit("listaPersonas", usuarios.getPersonasPorSala(data.sala));

        //callback(personas);
        callback(usuarios.getPersonasPorSala(data.sala));
    });

    client.on("crearMensaje", (data) => {
        let persona = usuarios.getPersona(client.id)

        let mensaje = crearMensaje(persona.nombre, data.mensaje);
        
        //io.sockets.in(persona.sala).emit("crearMensaje", mensaje);
        client.broadcast.emit("crearMensaje", mensaje);
        console.log("CREANDO MENSAJE");
        
    });

    client.on("crearMensaje", (data,callback) => {
        let persona = usuarios.getPersona(client.id)

        let mensaje = crearMensaje(persona.nombre, data.mensaje);
        
        //io.sockets.in(persona.sala).emit("crearMensaje", mensaje);
        //client.emit("crearMensaje",mensaje);
        
        callback(mensaje);
        console.log("CREANDO MENSAJE con callback");
    });


    client.on("disconnect", () => {
        let personaBorrada = usuarios.borrarPersona(client.id);

        client.broadcast.to(personaBorrada.sala).emit("crearMensaje", {
            nombre: "",
            mensaje: personaBorrada.nombre + " salio de la sala.",
        });
        client.broadcast.to(personaBorrada.sala).emit("listaPersonas", usuarios.getPersonas());
    });
    
    client.on("mensajePrivado", (data) => {
        let persona = usuarios.getPersona(client.id);

        client.broadcast
        .to(data.para)
        .emit("mensajePrivado", crearMensaje(persona.nombre, data.mensaje));
    });

    /*
    client.emit('enviarMensaje', {
        usuario: 'Administrador',
        mensaje: 'Bienvenido a esta aplicacion'
    })

    client.on("disconnect", () => {
        console.log("Usuario desconectado")
    });

    client.on("enviarMensaje", (data, callback) => {
        console.log(data);

        client.broadcast.emit("enviarMensaje", data);
    });
    */

});

server.listen(port, (err) => {

    if (err) throw new Error(err);

    console.log(`Servidor corriendo en puerto ${ port }`);

});

//socket.emit('crearMensaje', {mensaje: 'Hola a todos'})
